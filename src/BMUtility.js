

function Utility () {}

/**
 * Utility.getLabel: This method will return label.
 * @param fontName: name of the font
 * @param fontSize: size of the font
 * @param text: text of the label
 * @param position: position of teh label
 * @returns {*}
 */
Utility.getLabel = function (fontName, fontSize, text, position) {
    var label = new cc.LabelTTF(text, fontName, fontSize, cc.TEXT_ALIGNMENT_CENTER, cc.TEXT_ALIGNMENT_CENTER);
    label.setFontSize(fontSize);
    label.setPosition(cc.p(position.x, position.y));
    return label;
};
Utility.getMoveToAnimation  =   function (pTime, movement) {
    var moveTo  =  new  cc.MoveTo(0.1, cc.p(movement.x, movement.y));
    var easeBounceOut = new  cc.EaseBounceOut( moveTo );
    return easeBounceOut;
};

/**
 * Utility.getSprite: This method will return sprite.
 * @param spriteName: name of the sprite
 * @param position: position of teh sprite
 * @param tag: tag of the sprite.
 * @returns {spriteName}
 */
Utility.getSprite       =   function(spriteName, position, tag) {
    var sprite        =  cc.Sprite.create(spriteName);
    sprite.setPosition(cc.p(position.x, position.y));
    sprite.setTag(tag);
    return sprite;
};




