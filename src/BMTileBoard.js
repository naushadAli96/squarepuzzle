var SPTileBoard = cc.LayerColor.extend({
          mPositionXOffset : 0,
          mPositionYOffset : 0,
          mRow:0,
          mColumn:0,
           mInitialTag :0,
   ctor : function( pColor, pWidth, pHeight ){
      this._super( pColor, pWidth, pHeight );
      this.mBoardStatus = [[false, false, false, false, false],
           [false, false, false, false, false],
           [false, false, false, false, false],
           [false, false, false, false, false],
           [false, false, false, false, false],
          [false, false, false, false, false],
          [false, false, false, false, false]

       ];
      return true;
   },

   onEnter : function () {
      this._super();
   },

   onExit : function () {
      this._super();
   },

    getBoardStatus : function(pRows, pColumn){
       return this.mBoardStatus[pRows][pColumn];
    },
    UpdateBoardStatus : function(pIndex, status){
        this.mBoardStatus[ pIndex.x][ pIndex.y ] = status;

    },


   addTile : function( pColor, pRows, pColoum,  pTag){
              this.mRow = pRows;
              this.mColumn = pColoum;
              this.mInitialTag = pTag;
              this.addTiles(pColor, 0, 0, this.mInitialTag);
   },


    addTiles : function (pColor, pRows, pColumn) {
                var tile = new SPTile( pColor, CTileSize, CTileSize );
                tile.setTag(this.mInitialTag + (pColumn + (pRows * this.mColumn)));
                tile.setPosition( cc.p ( CTilesGap + this.mPositionXOffset, CTilesGap + this.mPositionYOffset ) );
                this.addChild( tile );
                    if(pRows < this.mRow) {
                        if (pColumn === (this.mColumn - 1)) {
                            pColumn = 0;
                            ++pRows;
                            this.mPositionYOffset += CTileSize + CTilesGap;
                            this.mPositionXOffset = 0;
                        } else {
                            this.mPositionXOffset += CTileSize + CTilesGap;
                            ++pColumn;
                        }

                        this.runAction(cc.sequence([new cc.DelayTime(0.05),
                            cc.callFunc(function () {
                                if(pRows < this.mRow)
                                this.addTiles(pColor, pRows, pColumn);
                            }, this)
                        ]));
                    }

    },














});