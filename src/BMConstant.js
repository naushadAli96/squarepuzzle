


var Tag_Background = 1254;

var NodeInitialTag = 1;


var BoardChildInitialTag = 100;
var NodeChildInitialTag = 1000;
var CTileBoardSize = cc.size(210, 262);
var CTileSize = 50;
var CTilesGap = 2;
var CRows = 5;
var CColumn = 4;

var GameMode =   {
    mEasyMode        :   1,
    mMediumMode      :   2,
    mHardMode        :   3,
    mExpertMode      :   4
};

var basePath                                              =       "res/BMLevelData/";
var Easy                                                  =       "BMEasyMode.json";
var Medium                                                =       "BMMediumMode.json";
var Hard                                                  =       "BMHardMode.json";
var Expert                                                =       "BMExpertMode.json";


//LocalStorageKey
var keyEasyMode                                           =       "EasyMode";
var keyMediumMode                                         =       "MediumMode";
var keyHardMode                                           =       "HardMode";
var keyExpertMode                                         =       "ExpertMode";


var keyCurrentMode                                        =        "CurrentMode";
var keyForCurrentLevel                                    =        "CurrentLevel";

var TileImage = [res.Tile1_png, res.Tile2_png, res.Tile3_png,res.Tile4_png, res.Tile5_png, res.Tile6_png];

var TotalLevel = 10;
var TotalMode = 4;
