

var BMLayer = cc.Layer.extend({
    ctor: function (pSize, pPosition, pTag) {
        this._super();
        this.customiseProperties(pSize, pPosition, pTag);
        return true;

    },
    onEnter : function () {
        this._super();
    },

    onExit : function () {
        this._super();
    },

    customiseProperties : function (pSize, pPosition, pTag) {
        this.setContentSize(pSize);
        this.setPosition(pPosition);
        this.setTag(pTag);
    },
     addBackGroundImage : function (pImage) {
       var bg =  Utility.getSprite(pImage, cc.p(this.getContentSize().width/2, this.getContentSize().height/2), Tag_Background);
      // bg.setPosition();
       this.addChild(bg);
     }

});