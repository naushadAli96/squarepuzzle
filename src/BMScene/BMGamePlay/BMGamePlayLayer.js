
var TagBMGamePlayLayer = 10000;
var BMGamePlayLayer = BMLayer.extend({
    mTileBoard : null,
    mTileBoardRect : null,
    mSize : null,
    touchListener :null,
    mSelectedNode : 0,
    mPlacedTileCount:0,
    isNodeSelected : false,
    mGameCompletionLayer:null,
    mNodeAPositions :[],
    mNodeBPositions: [],
    mNodeCPositions: [],
    mNodeDPositions: [],
    mNodeEPositions: [],
    mNodeFPositions: [],
    mTimeController: null,

    ctor : function () {
        this._super(cc.size(cc.winSize.width, cc.winSize.height), cc.p(0, 0), TagBMGamePlayLayer);
        this.mSize = cc.winSize;
        return true;
    },


    onEnter : function () {
        this._super();
        this.addBackGroundImage();
        this.addGameModeAndLevel();
        this.addTimerInGame();
        this.getLevelData();
        this.addGameComponent();
        this.addGameCompletionLayer();
         this.addTouchEvent();

    },

    getLevelData : function(){
        this.mLevelData = BMLevelDataHandler.getCurrentData(BMLevelController.getMode(), BMLevelController.getLevel());
    },

    addGameComponent : function(){
        this.updateModeAndLevel();
        this.addTileBoard();
        this.setNodePositions();
        this.addPickableTiles();
        this.mTimeController.startPlayTimer();
    },



    onExit : function () {
        this._super();
    },
    addTileBoard : function(){
        CTileBoardSize.width = (this.mLevelData.column * CTileSize) + ((this.mLevelData.column + 1) * CTilesGap);
        CTileBoardSize.height = (this.mLevelData.rows * CTileSize) + ((this.mLevelData.rows + 1) * CTilesGap);
        this.mTileBoard = new SPTileBoard(cc.color(255,255,255, 100), CTileBoardSize.width,  CTileBoardSize.height);
        this.mTileBoard.addTile(cc.color(0,0, 0,100),this.mLevelData.rows, this.mLevelData.column, BoardChildInitialTag);
        this.mTileBoard.setPosition(cc.p((this.mSize.width * 0.5) - this.mTileBoard.getContentSize().width/2, (this.mSize.height * 0.65) - this.mTileBoard.getContentSize().height/2));
       // this.setPosition
        this.mTileBoardRect = new cc.rect( this.mTileBoard.getPositionX(), this.mTileBoard.getPositionY(), this.getContentSize().width, this.getContentSize().height);
        this.addChild(this.mTileBoard);

    },


    addBackGroundImage : function(){
        var BG = new cc.Sprite(res.BG_JPEG);
        BG.setPosition(cc.winSize.width/2, cc.winSize.height/2);
        this.addChild(BG, 0);
    },



    addPickableTiles : function(){
        var tilePosition = [this.mNodeAPositions, this.mNodeBPositions, this.mNodeCPositions, this.mNodeDPositions, this.mNodeEPositions, this.mNodeFPositions];
        var nodePosition = [cc.p(50, 0), cc.p(50, 250), cc.p(300, 0), cc.p(250, 250), cc.p(300, 250)];
        var color = cc.color(0, 0, 0, 0); // ], cc.color(0, 255, 0, 255), cc.color(0, 0, 255, 255)];
        for(var index = 0; index < this.mLevelData.node; ++index){
            var node = new BMGroupedTile(color, tilePosition[index], this.mLevelData.tileSpriteIndex[index], NodeChildInitialTag);
            node.setPosition(cc.p(nodePosition[index].x, nodePosition[index].y));
            node.setTag(NodeInitialTag + index);
            node.mChildTileCount =  tilePosition[index].length;
            node.mInitialPosition = node.getPosition();
            this.addChild(node);
        }

    },

    addTouchEvent   : function() {
        this.touchListener  =   cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,

            onTouchBegan:function (touch, event) {
                var target                  =       event.getCurrentTarget();
                target.handleTouchBeganCall(cc.p(touch.getLocation().x, touch.getLocation().y));
                return true;
            },

            onTouchEnded : function(touch, event) {
                var target                  =       event.getCurrentTarget();
                target.handleTouchEndedCall();
            },

            onTouchMoved    :   function(touch, event) {
                var target                  =       event.getCurrentTarget();
                target.handleTouchMoveCall(cc.p(touch.getLocation().x, touch.getLocation().y));
            },

            onTouchCancelled    : function (touch, event) {
                var target                  =       event.getCurrentTarget();
                target.onTouchEnded(touch, event);
            }
        }, this);
    },
    /**
     * handleTouchEndedCall: This will handle touch end call.
     */
    handleTouchEndedCall    :   function () {

        if(this.isNodeSelected){
            this.checkBoxStatus();
        }
        this.isNodeSelected = false;

    },

    handleTouchMoveCall : function (position) {

        if(this.isNodeSelected) {
            var node = this.getChildByTag(this.mSelectedNode);
            var location = this.convertToNodeSpace(cc.p(position.x, position.y));
            var distanceToMoveInX = node.getPosition().x + location.x - this.mPreviousPosition.x;
            var distanceToMoveInY = node.getPosition().y + location.y - this.mPreviousPosition.y;
            node.setPosition(cc.p(distanceToMoveInX, distanceToMoveInY));
            this.mPreviousPosition = cc.p(location.x, location.y);
        }

    },


    handleTouchBeganCall : function (position)
    {
        this.checkSelectedNode(position);
    },

    setNodePositions: function(){
        this.clearNodePosition();
        for(var rows = 0; rows < this.mLevelData.rows; ++rows ){
            for(var column = 0; column < this.mLevelData.column; ++column){
                var position = cc.p((column*CTileSize) + ((column + 1) * CTilesGap),(rows*CTileSize) + ((rows + 1) * CTilesGap));
                switch(this.mLevelData.NodeArrangement[rows][column]){
                    case "A":
                        this.mNodeAPositions.push(position);
                        break;
                    case "B":
                        this.mNodeBPositions.push(position);
                        break;
                    case "C":
                        this.mNodeCPositions.push(position);
                        break;
                    case "D":
                        this.mNodeDPositions.push(position);
                        break;
                    case "E":
                        this.mNodeEPositions.push(position);
                        break;
                    case "F":
                        this.mNodeFPositions.push(position);
                        break;
                }
            }
        }
    },




    checkSelectedNode : function (position) {
        for (var index = 0; index < this.mLevelData.node; index++) {
            var tileNode = this.getChildByTag(NodeInitialTag + index);
            for (var childIndex = 0; childIndex < tileNode.mChildTileCount; ++childIndex) {
                var childTile = tileNode.getChildByTag(NodeChildInitialTag + childIndex);
                var tilePosition = cc.p(childTile.getPositionX() + tileNode.getPositionX(), childTile.getPositionY() + tileNode.getPositionY());//this.convertToWorldSpace(childTile.getPosition());
                var rect = new cc.rect(tilePosition.x, tilePosition.y, childTile.getContentSize().width, childTile.getContentSize().height);
                if (cc.rectContainsPoint(rect, position)) {
                    this.mSelectedNode = NodeInitialTag + index;
                    tileNode.setScale(1.0);
                    tileNode.setLocalZOrder(10);
                    if(tileNode.mIsPlacedInsideBoard){
                        this.mPlacedTileCount -= 1;
                        tileNode.mIsPlacedInsideBoard = false;
                        tileNode.setLocalZOrder(10);
                        this.UpdateBoard(tileNode, false);
                    }
                    this.isNodeSelected = true;
                    this.mPreviousPosition = position;

                    return;
                }

            }
        }
    },


    checkBoxStatus : function(){
        var node = this.getChildByTag(this.mSelectedNode);
        var xMin = -CTileSize/2;
        var xMax = ((this.mLevelData.column * CTileSize) + (this.mLevelData.column + 1) * CTilesGap) - CTileSize/2;
        var yMin = -CTileSize/2;
        var yMax = ((this.mLevelData.rows * CTileSize) + (this.mLevelData.rows + 1) * CTilesGap) - CTileSize/2;

        for(var tileIndex =0; tileIndex < node.mChildTileCount; ++tileIndex ){
            var xOffset = (node.mPositions[tileIndex].x + node.getPositionX() - this.mTileBoard.getPositionX());
            var yOffset = (node.mPositions[tileIndex].y + node.getPositionY() - this.mTileBoard.getPositionY());

            var column = Math.round((node.mPositions[tileIndex].x + node.getPositionX() - this.mTileBoard.getPositionX()) / CTileSize);
            var rows  =  Math.round((node.mPositions[tileIndex].y + node.getPositionY() - this.mTileBoard.getPositionY()) /CTileSize);

            if(xOffset < xMin || xOffset > xMax
                || yOffset < yMin || yOffset > yMax
                || column < 0 || column > this.mLevelData.column
                || rows < 0 || rows > this.mLevelData.rows
                || ((this.mTileBoard.getBoardStatus( Math.min(Math.max(0, rows), this.mLevelData.rows  - 1),  Math.min(Math.max(0, column), this.mLevelData.column - 1))))
            ){
                node.runAction( Utility.getMoveToAnimation(0.2, node.mInitialPosition));
                return;
            }
        }

        var margin = this.GetXandYmargin(xOffset, yOffset);
        node.setPosition(cc.p(node.getPositionX() + margin.x , node.getPositionY() + margin.y));
        node.mIsPlacedInsideBoard = true;
        node.setLocalZOrder(0);
        this.mPlacedTileCount += 1;
        this.UpdateBoard(node, true);
        this.CheckWin();
    },
    CheckWin : function(){
        if(this.mPlacedTileCount == this.mLevelData.node){
            this.enableGameCompletion();
            this.mTimeController.stopTimer();
        }
    },

    updateLevel : function(){
        this.RemoveTileBoardAndPosition();
        this.mPlacedTileCount = 0;
        this.mLevelData = null;
        var level = BMLevelController.getLevel();
        level = level + 1;
        BMLevelController.setLevel(level);
        this.getLevelData();
        this.addGameComponent();
    },


    UpdateBoard : function(pNode, pStatus){
        for(var tileIndex =0; tileIndex < pNode.mChildTileCount; ++tileIndex ){
            var column = Math.round((pNode.mPositions[tileIndex].x + pNode.getPositionX() - this.mTileBoard.getPositionX()) / CTileSize);
            var rows  =  Math.round((pNode.mPositions[tileIndex].y + pNode.getPositionY() - this.mTileBoard.getPositionY()) /CTileSize);
            this.mTileBoard.UpdateBoardStatus( cc.p( Math.min(Math.max(0, rows), this.mLevelData.rows),  Math.min(Math.max(0, column), this.mLevelData.column)) , pStatus);

        }
    },



    GetXandYmargin : function(pXPos, pYpos){
        var xOffset;
        var yOffset;
        var tileXPos = 2, tileYPos = 2;
        while(Math.abs(tileXPos - pXPos) > 25){
            tileXPos += 52;
        }

        xOffset = tileXPos - pXPos;
        while(Math.abs(tileYPos - pYpos) > 25){
            tileYPos += 52;
        }
        yOffset = tileYPos - pYpos;
        return cc.p(xOffset,yOffset);
    },
    addGameModeAndLevel : function () {
        /************** Level ***************/

        var gameLevelText = Utility.getLabel(lemonMilkFont, 40, "Level:", cc.p(cc.winSize.width*0.75, this.getContentSize().height * 0.95));
        gameLevelText.setAnchorPoint(cc.p(0, 0));
        this.addChild(gameLevelText);
        this.mGameLevel = Utility.getLabel(lemonMilkFont, 40, "0", cc.p(gameLevelText.getPositionX() + gameLevelText.getContentSize().width * 1.1, this.getContentSize().height * 0.95));
        this.mGameLevel.setAnchorPoint(cc.p(0, 0));
        this.addChild(this.mGameLevel);



    },

    updateModeAndLevel : function () {
        this.mGameLevel.setString(BMLevelController.getLevel().toString());

    },

    RemoveTileBoardAndPosition : function () {
        if (this.mTileBoard != null)
            this.removeChild(this.mTileBoard);
        for(var nodeIndex = 0; nodeIndex < this.mLevelData.node; ++nodeIndex){
            var node = this.getChildByTag(NodeInitialTag + nodeIndex);
            if(node != null){
                node.resetPosition();
                this.removeChild(node);
            }


        }

        /*********************** Position ***************/
           this.clearNodePosition();
    },



    clearNodePosition:function(){
        this.mNodeAPositions.length = 0;
        this.mNodeBPositions.length = 0;
        this.mNodeCPositions.length = 0;
        this.mNodeDPositions.length = 0;
        this.mNodeEPositions.length = 0;
        this.mNodeDPositions.length = 0;
    },
    /************************* Game Completion **********************/
    addGameCompletionLayer : function () {
        this.mGameCompletionLayer = new BMGameCompletionLayer();
        this.mGameCompletionLayer.setDelegate(this);
        this.mGameCompletionLayer.setVisible(false);
        this.mGameCompletionLayer.setLocalZOrder(100);
        this.addChild(this.mGameCompletionLayer);


    },

    enableGameCompletion : function () {
        this.mGameCompletionLayer.setVisible(true);
        this.mGameCompletionLayer.addTextAndBG(this.mTimeController.mTimer.getString());
    },
     disableGameCompletion : function () {
         this.mGameCompletionLayer.setVisible(false);
     },
    LevelCompletionNoticeRemoved : function () {
        this.disableGameCompletion();
        this.updateLevel();
    },


    /***********************************************************************/

    addTimerInGame: function () {
        this.mTimeController = new BMTimeController();
        this.addChild(this.mTimeController);
    }




});
