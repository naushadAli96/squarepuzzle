var TagBackButton = 500;
var BMGamePlayScene = cc.Scene.extend({
    ctor: function () {
        this._super();
        return true;
    },

    onEnter : function () {
        this._super();
        this.addGamePlayLayer();
        BMSoundManager.playMusic(res.BackgroundMusic, true);

    },

    onExit : function () {
        BMSoundManager.stopMusic();
        this._super();
    },

    addGamePlayLayer :function(){
        var gamePlay = new BMGamePlayLayer();
        this.addChild(gamePlay);
        this.addBackButton();
    },

    addBackButton : function () {
        var backButton = new BMButton(res.BackButton, cc.p(cc.winSize.width * 0.1, cc.winSize.height*0.95), TagBackButton);
        backButton.setDelegate(this);
        backButton.setScale(0.75);
        this.addChild(backButton);

    },
    buttonPressed : function (pSender) {
        BMSoundManager.playSound(res.ButtonSound, false);
        cc.director.runScene(new  cc.TransitionSlideInL(0.5, new BMModeSelectionScene()));
    },


});