 cc.GameCompletionDelegate = cc.Class.extend({
     LevelCompletionNoticeRemoved : function () {

     }
 });
var Tag_LevelCompletion = 1263;
var BMGameCompletionLayer = BMLayer.extend({
    mBG : null,
    mLevelCompleted:null,
    mDelegate:null,
    mTimer: null,
 ctor: function () {
     this._super(cc.winSize, cc.p(0, 0), Tag_LevelCompletion);
     return true;
 },
    onEnter : function(){
        this._super();
        this.addBlurLayer();
        this.addBackGroundImage(res.GameCompleteBG);
        this.getBG();
        this.mLevelCompleted = Utility.getLabel(lemonMilkFont, 60, "Level Completed", cc.p(-200, cc.winSize.height/2));
        this.addChild(this.mLevelCompleted);
        this.addGameCompletionTime();
    },

    onExit : function () {
        this._super();
    },

    getBG : function(){
        this.mBG  = this.getChildByTag(Tag_Background);
        this.mBG.setPosition(cc.winSize.width/2, cc.winSize.height * 1.2);
    },
    addBlurLayer : function(){
        var blurLayer = new cc.LayerColor(cc.color(0, 0, 0, 200), cc.winSize.width, cc.winSize.height);
        this.addChild(blurLayer);
    },

    addGameCompletionTime : function(){
        this.mTimer = Utility.getLabel(lemonMilkFont, 40, "0:0", cc.p(this.mBG.getContentSize().width*0.5, this.mBG.getContentSize().height * 0.35));
        this.mBG.addChild(this.mTimer);
    },

    addTextAndBG : function(pTimer){
        this.addBGInLayer();
        this.addLevelCompletedText();
        this.mTimer.setString( "Time: " + pTimer);
    },

    addBGInLayer : function () {

        this.mBG.runAction(cc.sequence([
            cc.moveTo(1.0, this.getContentSize().width * 0.5 , cc.winSize.height * 0.5), new  cc.DelayTime(2.0),
            cc.callFunc(function(){
                this.moveBGFromLayer();
            }, this)
        ]));
    },
    moveBGFromLayer : function () {

        this.mBG.runAction(cc.sequence([
            cc.moveTo(0.5, this.getContentSize().width * 0.5 , cc.winSize.height * 1.2), new  cc.DelayTime(0.2),
            cc.callFunc(function(){
                this.mLevelCompleted.stopAllActions();
                this.mDelegate.LevelCompletionNoticeRemoved();
            }, this)
        ]));
    },

    setDelegate : function(pRef){
        this.mDelegate = pRef;
    },

    addLevelCompletedText: function () {
      this.mLevelCompleted.runAction(cc.sequence([
          cc.moveTo(1.0, this.getContentSize().width * 0.5 , cc.winSize.height * 0.55), new  cc.DelayTime(2.0),
          cc.callFunc(function(){
            this.removeLevelText();
          }, this)
      ]));
    },

    removeLevelText : function () {
        this.mLevelCompleted.runAction(cc.sequence([
            cc.moveTo(0.5, this.getContentSize().width *1.5 , cc.winSize.height * 0.55), new  cc.DelayTime(0.2),
            cc.callFunc(function(){
                this.mLevelCompleted.setPosition(cc.p(-200, cc.winSize.height/2));
                this.mLevelCompleted.stopAllActions();
            }, this)
        ]));
    }

});