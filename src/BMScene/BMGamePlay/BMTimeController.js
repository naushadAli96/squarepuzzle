var frameCount = 0;
var BMTimeController = cc.Layer.extend({
    mTimer: null,
    mTimerValue:null,
    mIsTimerOn : false,
    ctor: function () {
       this._super();
       this.addTimer();
       return true;
    },
     addTimer : function(){
        this.mTimer = Utility.getLabel(lemonMilkFont, 40, "00", cc.p(cc.winSize.width * 0.5, cc.winSize.height * 0.95 ));
        this.addChild(this.mTimer);
     },

    startPlayTimer :function () {
        this.scheduleUpdate();
        this.mIsTimerOn = true;
        this.mTimerValue = 0;
    },

    stopTimer : function () {
        this.mIsTimerOn =  false;
        this.unscheduleUpdate();
    },

    resetTimer : function () {
        this.mTimerValue = 0;
    },

    getTimeString : function(){
      var minute = this.mTimerValue/60;
      var second = this.mTimerValue%60;
      return minute <= 0 ? this.mTimerValue : minute.toFixed(0) +":"+ second.toFixed(0);
    },

    update : function (dt) {
        if(this.mIsTimerOn) {
            if (frameCount > 60) {
                ++this.mTimerValue;
                frameCount = 0;
            }
            ++frameCount;
            this.mTimer.setString(this.getTimeString());
        }
    }
});