var TagBackButton = 500;
var BMModeSelectionScene = cc.Scene.extend({
    ctor: function () {
        this._super();
        return true;
    },

    onEnter : function () {
        this._super();
        this.addModeSelectionLayer();
        this.addBackButton();

    },

    addModeSelectionLayer : function(){
        var modeSelection = new BModeSelectionLayer();
        modeSelection.setPosition(cc.p(0, 0));
        modeSelection.setDelegate(this);
        this.addChild(modeSelection);
    },

    ModeSelectionLayerCallback : function (pGameModeType) {
        BMLevelController.setMode(pGameModeType);
        this.GoToGamePlayScene();
    },

    GoToGamePlayScene : function() {
        cc.director.runScene( new  cc.TransitionSlideInR(0.5, new BMGamePlayScene()));
    },

    onExit : function () {
        this._super();
    },

    addBackButton : function () {
        var backButton = new BMButton(res.BackButton, cc.p(cc.winSize.width * 0.1, cc.winSize.height*0.95), TagBackButton);
        backButton.setDelegate(this);
        backButton.setScale(0.75);
        this.addChild(backButton);

    },
    buttonPressed : function (pSender) {
        BMSoundManager.playSound(res.ButtonSound, false);
        cc.director.runScene(new  cc.TransitionSlideInL(0.5, new BMHomeScene()));
    },


});