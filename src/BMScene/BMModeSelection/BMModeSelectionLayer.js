
var CGameModeCount = 4;

var TagModeSelection = 5000;
var ModeInitialTag = 6000;

var buttonText = ["Easy", "Medium" , "Hard", "Expert"];
var lemonMilkFont = "res/Font/LemonMilk.otf";




cc.BModeSelectionLayerDelegate = cc.Class.extend({

    ModeSelectionLayerCallback: function (sender) {

    }
});


var BModeSelectionLayer =  BMLayer.extend({
      mTitleText : null,
      mDelegate : null,
      mXPos : cc.winSize.width * 1.5,
      mYPos : cc.winSize.height * 0.6,
   ctor: function () {
       this._super(cc.size(cc.winSize.width, cc.winSize.height ), cc.p(0,0), TagModeSelection);
       return true;
   },

   onEnter : function () {
       this._super();
       this.addBackGroundImage(res.BG_JPEG);
      // this.addModeButton();
       this.animateButton(0);
       this.yPos = cc.winSize.height * 0.6;
       this.addTitleText();
   },

   onExit :function () {
       this._super();
   },


    addTitleText : function () {
         this.mTitleText = Utility.getLabel(lemonMilkFont, 50, "Select Game Mode", cc.p(this.getContentSize().width * 0.5, this.getContentSize().height * 1.2));
        // this.mTitleText.setAnchorPoint(cc.p(0.0, 0.0));
        this.mTitleText.runAction ( new cc.moveTo(0.4, cc.p(this.getContentSize().width * 0.5, this.getContentSize().height * 0.85)));

         this.addChild( this.mTitleText);
    },

    animateButton : function (pIndex) {
        var modeButton = new BMButton(res.ModeBG_png, cc.p(this.mXPos, this.mYPos ), ModeInitialTag + pIndex);
        var text = Utility.getLabel(lemonMilkFont, 50, buttonText[pIndex], cc.p(150,36));
        modeButton.addChild(text);
        modeButton.setDelegate(this);
        modeButton.runAction(cc.sequence([
            Utility.getMoveToAnimation(1.5, cc.p(this.getContentSize().width * 0.5 ,this.mYPos)), new  cc.DelayTime(0.2),
            cc.callFunc(function(){
                if(++pIndex < 4){
                    this.mYPos -=  modeButton.getContentSize().height * 1.5;
                    this.animateButton(pIndex);
                }
            }, this)
        ]));
        this.addChild(modeButton);

    },

    buttonPressed : function (pSender) {
        BMSoundManager.playSound(res.ButtonSound, false);
          var selectedGameMode = GameMode.mEasyMode;
          switch(pSender.getTag() - ModeInitialTag){
              case 1:
                  selectedGameMode = GameMode.mMediumMode;
                  break;
              case 2:
                  selectedGameMode = GameMode.mHardMode;
                  break;
              case 3:
                  selectedGameMode = GameMode.mHardMode;
          }
        this.mDelegate.ModeSelectionLayerCallback(selectedGameMode);
    },
    setDelegate : function(pRef){
          this.mDelegate = pRef;
    }



});