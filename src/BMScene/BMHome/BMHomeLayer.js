var Tag_HomeLayer = 2000;
var Tag_PlayButton = 2001;


cc.BMHomeLayerDelegate = cc.Class.extend({

    HomeLayerCallback: function (sender) {

    }
});


var BMHomeLayer =  BMLayer.extend({
    mDelegate : null,
    mPlayButton : null,
    mGameName : null,
  ctor : function (){
      this._super(cc.size(cc.winSize.width, cc.winSize.height), cc.p(0,0), Tag_HomeLayer);
      return true;
 },
    onEnter : function () {
      this._super();
      this.addBackGroundImage(res.BG_JPEG);
      this.addPlayButton();
      this.addGameName();

    },

    addPlayButton :  function(){
        this.mPlayButton =  new BMButton(res.Play_png, cc.p(this.getContentSize().width/2, this.getContentSize().height * 0.4), Tag_PlayButton);
        this.mPlayButton.setDelegate(this);
        this.mPlayButton.setScale(0.5);
        this.mPlayButton.runAction ( new cc.RepeatForever( new cc.sequence(new cc.scaleTo(0.2, 0.4, 0.4), new cc.scaleTo(0.2, 0.35, 0.35) ,new cc.DelayTime(3.0) ) ) );
     this.addChild( this.mPlayButton);
    },

    addGameName : function (){

        this.mGameName = Utility.getLabel("res/Font/LemonMilk.otf", 70, "Block Master", cc.p(this.getContentSize().width * 0.5, this.getContentSize().height * 1.2));

        this.mGameName.runAction ( new cc.moveTo(0.7, cc.p(this.getContentSize().width * 0.5, this.getContentSize().height * 0.7)));
         this.addChild( this.mGameName);
    },

    setDelegate : function(pRef){
      this.mDelegate = pRef;
    },


    buttonPressed: function(sender){
        BMSoundManager.playSound(res.ButtonSound, false);
        this.HidePlayButton();
        this.ResetGameNameToInitialPosition();
        this.mDelegate.HomeLayerCallback(sender);
    },

    HidePlayButton : function () {
        this.mPlayButton.stopAllActions();
        this.mPlayButton.setScale(0.5);
        this.mPlayButton.setVisible(false);

    },

    ResetGameNameToInitialPosition : function () {
        this.mGameName.setPosition( cc.p(this.getContentSize().width * 0.5, this.getContentSize().height * 1.2));
        this.mGameName.setVisible(false);
    }


});