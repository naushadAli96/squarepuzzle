var BMHomeScene = cc.Scene.extend({
    ctor :  function () {
        this._super();
        return true;
    },

    onEnter : function () {
        this._super();
        this.addHomeLayer();
    },

    onExit : function () {
        this._super();
    },

    addHomeLayer : function () {
       var homeLayer = new BMHomeLayer();
       homeLayer.setDelegate(this);
       this.addChild(homeLayer);
    },

    HomeLayerCallback : function (sender) {
        cc.director.runScene(new  cc.TransitionSlideInR(0.5, new BMModeSelectionScene()));
    },



});