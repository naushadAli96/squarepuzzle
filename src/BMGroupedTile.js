var BMGroupedTile = cc.Node.extend({
    touchListener : null,
    mChildTileCount :0,
    mInitialPosition : cc.p(0,0),
    mPositions:null,
    mIsPlacedInsideBoard : false,
    mPositionOnBoard : [],
    ctor:function (pColor, pPositionsArray, pImageIndex, pTag) {
       this.mChildTileCount = pPositionsArray.length;
       this.mPositions = pPositionsArray.slice(0);
        this._super();
        this.arrangeTile(pColor, pPositionsArray, pImageIndex, pTag);
        return true;
    },

    arrangeTile : function(pColor, pPositionsArray, pImageIndex, pTag){
        for(var index = 0; index < pPositionsArray.length; ++index){
            var tile = new SPTile(pColor, CTileSize, CTileSize);
            tile.addImage(pImageIndex);
            tile.setTag(pTag + index);
            tile.setPosition(cc.p(pPositionsArray[index].x, pPositionsArray[index].y));
            this.addChild(tile);
        }
    },

    resetPosition : function () {
        this.mPositions.length = 0;
    }

});