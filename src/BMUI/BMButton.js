
cc.BMButtonDelegate = cc.Class.extend({

    buttonPressed: function (sender) {

    }
});


var BMButton = ccui.Button.extend({

    mDelegate: null,
    ctor: function (pButtonImage, pPosition, pTag) {
        this._super();
        this.customiseProperties(pButtonImage, pPosition, pTag);
        return true;
    },

    onEnter: function () {
        this._super();

    },

    customiseProperties: function (pButtonImage, pPosition, pTag) {
        this.loadTextureNormal(pButtonImage, ccui.Widget.LOCAL_TEXTURE);
        this.setPosition(cc.p(pPosition.x, pPosition.y));
        this.setTag(pTag);
        this.addTouchEventListener(this.buttonCallback, this);
    },

    /**
     * setDelegate : this will set the delegate for button
     * @param ref
     */
    setDelegate: function (ref) {
        this.mDelegate = ref;
    },

    /**
     * buttonCallback : This is a callback method of button touch event which will call when a button will be touch.
     * This will call a delegate method button pressed when a button will be touched.
     * @param sender: button which will be touch.
     * @param type: type of the touch event
     */
    buttonCallback: function (sender, type) {

        switch (type) {
            case ccui.Widget.TOUCH_BEGAN:
                this.mDelegate.buttonPressed(sender);
                break;
        }
    },

    onExit: function () {
        this._super();
    },
});