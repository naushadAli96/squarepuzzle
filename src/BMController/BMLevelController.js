var BMLevelController = (function () {

    function setGameMode(pGameMode) {
        if(pGameMode == TotalMode && getCurrentLevel() == TotalLevel){
            setGameMode(GameMode.mEasyMode);
            setCurrentLevel(0);
            return;
        }
        localStorage.setItem(keyCurrentMode, pGameMode);
    }

    function getGameMode() {
       return localStorage.getItem(keyCurrentMode) ;
    }

    function setCurrentLevel(pLevel) {

        var keyCurrentLevel = getGameMode() + keyForCurrentLevel;
        if(pLevel === TotalLevel){
            setGameMode(getGameMode() + 1);
        }
        localStorage.setItem(keyCurrentLevel, pLevel);
    }

    function getCurrentLevel() {
        var keyCurrentLevel = getGameMode() + keyForCurrentLevel;
         return parseInt(  localStorage.getItem(keyCurrentLevel) == null ? 0 : localStorage.getItem(keyCurrentLevel));
    }



    return {
        setMode                 :   setGameMode,
        getMode                 :   getGameMode,
        setLevel                :   setCurrentLevel,
        getLevel                :   getCurrentLevel,
    };

})();
