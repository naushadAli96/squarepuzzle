

var BMLevelDataHandler = (function () {

    function load(pGamemode) {
        var key         =   getKey(pGamemode);
        if(localStorage.getItem(key) == null) {
            cc.loader.loadJson(getModeName(pGamemode),function(error, data){
                set(data, pGamemode);
            });
        }
    }

    function getModeName(pGameMode) {
        var path        =   basePath + Easy;
        switch (pGameMode) {
            case GameMode.mMediumMode:
                path    =   basePath + Medium;
                break;

            case GameMode.mHardMode:
                path    =   basePath + Hard;
                break;

            case GameMode.mExpertMode:
                path    =  basePath + Expert;
                break;
        }
        return path;
    }

    function getKey(gameMode) {
        var key    =   keyEasyMode;
        switch (gameMode) {
            case GameMode.mMediumMode:
                key    =   keyMediumMode;
                break;

            case gameMode.mHardMode:
                key    =   keyHardMode;
                break;

            case GameMode.mExpertMode:
                key    =   keyExpertMode;
                break;
        }
        return key;
    }

    function get(pGameMode) {
        var data    =   localStorage.getItem(getKey(pGameMode));
        if(data !== null) {
            data        =   JSON.parse(data);
        }
        return data;
    }

    function getCurrentData(pGameMode, counter ) {
        var data = localStorage.getItem(getKey(pGameMode));
        data        =   JSON.parse(data);

        return  data[counter];
    }

    function set(pGameMode, type) {
        var key         =   getKey(type);
        localStorage.setItem(key, JSON.stringify(pGameMode));
    }
    return {
        load                    :   load,
        get                     :   get,
        getCurrentData          :   getCurrentData,
        set                     :   set
    };

})();


