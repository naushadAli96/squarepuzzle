
var BMSoundManager = (function () {


    return {

        playSound : function (soundName, isLoopRequired) {

            cc.audioEngine.setEffectsVolume(0.2);
            cc.audioEngine.playEffect(soundName, isLoopRequired);
        },

        playMusic : function (musicName, isLoopRequired) {
            cc.audioEngine.playMusic(musicName, isLoopRequired);
        },

        stopMusic : function() {
            cc.audioEngine.stopMusic(true);
        },

    };

})();