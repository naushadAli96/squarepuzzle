/****************************************************************************
 Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.
 
 http://www.cocos2d-x.org
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

var res = {
    HelloWorld_png : "res/HelloWorld.png",
    BG_JPEG        :  "res/UI/BG.jpg",
    Play_png       :  "res/UI/play.png",
    ModeBG_png     :   "res/UI/ModeBG.png",
    GameCompleteBG :    "res/UI/GameCompletedBG.jpg",
    BackButton     :    "res/UI/BackButton.png",
    Tile1_png      :    "res/BMTile/Tile_1.png",
    Tile2_png      :    "res/BMTile/Tile_2.png",
    Tile3_png      :    "res/BMTile/Tile_3.png",
    Tile4_png      :    "res/BMTile/Tile_4.png",
    Tile5_png      :    "res/BMTile/Tile_5.png",
    Tile6_png      :    "res/BMTile/Tile_6.png",
    ButtonSound    :    "res/BMSound/ButtonClicked.mp3",
    BackgroundMusic:    "res/BMSound/BackgroundMusic.mp3",
};

var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
}
