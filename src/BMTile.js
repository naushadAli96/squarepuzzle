var spriteTag = 10000;
var SPTile = cc.LayerColor.extend({
   ctor : function (pColor, pWidth, pHeight) {
       this._super(pColor, pWidth, pHeight);
       return true;
   },

    addImage : function (pImageIndex) {
        var sprite = Utility.getSprite(TileImage[pImageIndex], cc.p(CTileSize/2, CTileSize/2), spriteTag);
        this.addChild(sprite);
    }

});